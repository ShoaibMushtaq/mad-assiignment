package com.example.assignment;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.os.Bundle;

import com.hololo.tutorial.library.TutorialActivity;
import android.Manifest;
import android.graphics.Color;
import android.os.Bundle;
import android.widget.Toast;

import com.hololo.tutorial.library.PermissionStep;
import com.hololo.tutorial.library.Step;
import com.hololo.tutorial.library.TutorialActivity;

public class Onboardingscreen extends TutorialActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        addFragment(new Step.Builder().setTitle("This is header")
                .setContent("This is content")
                .setBackgroundColor(Color.parseColor("#FF0957")) // int background color
                .setDrawable(R.drawable.imgonee) // int top drawable
                .setSummary("This is summary")
                .build());


            addFragment(new Step.Builder().setTitle("This is header")
                    .setContent("This is content")
                    .setBackgroundColor(Color.parseColor("#FF0957")) // int background color
                    .setDrawable(R.drawable.imgtwo) // int top drawable
                    .setSummary("This is summary")
                    .build());


                addFragment(new Step.Builder().setTitle("This is header")
                        .setContent("This is content")
                        .setBackgroundColor(Color.parseColor("#FF0957")) // int background color
                        .setDrawable(R.drawable.imgthree) // int top drawable
                        .setSummary("This is summary")
                        .build());
    }




    @Override
    public void finishTutorial() {
        Toast.makeText(this, "Tutorial finished", Toast.LENGTH_SHORT).show();
        finish();
    }

    @Override
    public void currentFragmentPosition(int position) {
        Toast.makeText(this, "Position : " + position, Toast.LENGTH_SHORT).show();
    }
}
