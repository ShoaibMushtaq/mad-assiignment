package com.example.assignment;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.regex.Pattern;


public class MainActivity extends AppCompatActivity {

    DatabaseHelper db;
    EditText tr1,tr2;
    Button b2;
    TextView tvSignUp;

//    EditText emailId, password;
    //  Button btnSignIn;
    //  TextView tvSignUp;
    //  FirebaseAuth mFirebaseAuth;
    // private FirebaseAuth.AuthStateListener mAuthStateListener;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        db =  new DatabaseHelper (this);
        tr1 = (EditText)findViewById(R.id.Tr_Email);
        tr2 = (EditText)findViewById(R.id.Tr_password);
        b2 = (Button) findViewById(R.id.Tr_SignIn);
        tvSignUp = findViewById(R.id.textView);
        tvSignUp.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                String s1 = tr1.getText().toString();
                String s2 = tr2.getText().toString();

                if (s1.equals("") || s2.equals("") ) {
                    Toast.makeText(getApplicationContext(), "Feilds are empty", Toast.LENGTH_SHORT).show();
                } else {
                    if (s1.equals(s2)) {
                        Boolean chkemail = db.chkemail(s1);
                        if (chkemail == true) {
                            Boolean insert = db.insert(s1, s2);
                            if (insert == true) {
                                Toast.makeText(getApplicationContext(), "Login Succesfully", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(getApplicationContext(), "Wrong email or password", Toast.LENGTH_SHORT).show();
                        }
                    }
                }

            }

        });
        tvSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this,ass2.class);
                startActivity(i);
            }
        });
    }

}


        //   mFirebaseAuth = FirebaseAuth.getInstance();
        //  emailId = findViewById(R.id.Tr_Email);
        // password = findViewById(R.id.Tr_password);
        //   btnSignIn = findViewById(R.id.Tr_SignIn);
        //  tvSignUp = findViewById(R.id.textView);

        //  mAuthStateListener = new FirebaseAuth.AuthStateListener() {
        //    @Override
        ///   public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
        //    FirebaseUser mFirebaseUser = mFirebaseAuth.getCurrentUser();
        //  if( mFirebaseUser != null ){
        //    Toast.makeText(Login.this,"You are logged in",Toast.LENGTH_SHORT).show();
        //  Intent i = new Intent(Login.this, Homescreen.class);
        //startActivity(i);
        //}
        // else{
        //   Toast.makeText(Login.this,"Please Login",Toast.LENGTH_SHORT).show();
        //}
        // }
        //  };

        //btnSignIn.setOnClickListener(new View.OnClickListener() {
        //  @Override
        //public void onClick(View v) {
        //  String email = emailId.getText().toString();
        //String pwd = password.getText().toString();
        //if(email.isEmpty()){
        //  emailId.setError("Please enter email id");
        //emailId.requestFocus();
        // }
        //             else  if(pwd.isEmpty()){
        ///               password.setError("Please enter your password");
        //            password.requestFocus();
        //      }
        //    else  if(email.isEmpty() && pwd.isEmpty()){
        //      Toast.makeText(Login.this,"Fields Are Empty!",Toast.LENGTH_SHORT).show();
        //              }
        //          else  if(!(email.isEmpty() && pwd.isEmpty())){
        //                mFirebaseAuth.signInWithEmailAndPassword(email, pwd).addOnCompleteListener(Login.this, new OnCompleteListener<AuthResult>() {
        //                @Override
        ///              public void onComplete(@NonNull Task<AuthResult> task) {
        ///               if(!task.isSuccessful()){
        ///                Toast.makeText(Login.this,"Login Error, Please Login Again",Toast.LENGTH_SHORT).show();
        ///         }
        //      else{
        //        Intent intToHome = new Intent(Login.this,Homescreen.class);
        //      startActivity(intToHome);
        // }
        // }
        //  });
        // }
        //  else{
        //     Toast.makeText(Login.this,"Error Occurred!",Toast.LENGTH_SHORT).show();

        //       }

        //}
        // });

        // tvSignUp.setOnClickListener(new View.OnClickListener() {
        //   @Override
        // public void onClick(View v) {
        //   Intent intSignUp = new Intent(Login.this, MainActivity.class);
        // startActivity(intSignUp);
//            }
        //      });
        // }

        //   @Override
        /// protected void onStart() {
        ///  super.onStart();
        //  mFirebaseAuth.addAuthStateListener(mAuthStateListener);
        // }
